# aws_timestreamwrite_database
resource "aws_timestreamwrite_database" "basic-timestream-db" {
  database_name = "basic-timestream-db"
}

# aws_timestreamwrite_table linked to the database
resource "aws_timestreamwrite_table" "basic-timestream-table" {
  database_name = aws_timestreamwrite_database.basic-timestream-db.database_name
  table_name    = "temperaturesensor"
}
